import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import javax.naming.OperationNotSupportedException;

import static org.junit.jupiter.api.Assertions.*;

class EquationProcessorTest {
    static EquationProcessor equationProcessor;
    @BeforeAll
    static void init(){
        equationProcessor = new EquationProcessor();
    }
    @Test
    @DisplayName("basic equation")
    void basic_equation() throws OperationNotSupportedException {
        String equation = "1+1";
        String result = "2.0";
        assertEquals(result,equationProcessor.execute(equation));
    }
    @Test
    void basic_equation_with_parenthesis() throws OperationNotSupportedException {
        String equation = "(1+1)";
        String result = "2.0";
        assertEquals(result,equationProcessor.execute(equation));
    }
}