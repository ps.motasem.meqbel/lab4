import java.util.regex.Pattern;

public class EquationCorrector {
    public static void main(String[] args) {
        System.out.println(new EquationCorrector().correct("(1+3)-(2*3)/10"));
    }
    public String correct(String equation) {
        if(equation.isEmpty())
            return equation;
        StringBuilder stringBuilder = new StringBuilder();
        appendFirstChar(equation, stringBuilder);
        for (int i = 1; i < equation.length()-1; i++) {
            char currentCharacter = equation.charAt(i);
            if(isSpace(currentCharacter)) continue;
            processChar(equation, stringBuilder, i, currentCharacter);
        }
        appendLastChar(equation, stringBuilder);
        return stringBuilder.toString();
    }
    private void processChar(String equation, StringBuilder stringBuilder, int i, char currentCharacter) {
        if(isParentheses(currentCharacter)) {
            handleParenthesis(equation, stringBuilder, i, currentCharacter);
            return;
        }
        stringBuilder.append(currentCharacter);
    }
    private void appendLastChar(String equation, StringBuilder stringBuilder) {
        int lastCharIndex = equation.length()-1;
        char lastChar = equation.charAt(lastCharIndex);
        stringBuilder.append(lastChar);
    }
    private void appendFirstChar(String equation, StringBuilder stringBuilder) {
        char firstChar = equation.charAt(0);
        stringBuilder.append(firstChar);
    }
    private boolean isSpace(char c) {
        return c == ' ';
    }
    private boolean isParentheses(char element) {
        return Pattern.matches("[()]",element+"");
    }
    private void handleParenthesis(String equation, StringBuilder stringBuilder, int i, char currentCharacter) {
        if(isLeftParenthesis(currentCharacter)) {
            handleLeftParenthesis(equation, stringBuilder, i, currentCharacter);
            return;
        }
        handleRightParenthesis(equation, stringBuilder, i, currentCharacter);
    }
    private void handleRightParenthesis(String equation, StringBuilder stringBuilder, int i, char currentCharacter) {
        stringBuilder.append(currentCharacter);
        char nextChar = equation.charAt(i+1);
        if(isDigit(nextChar)){
            stringBuilder.append('*');
        }
    }
    private boolean isDigit(char value) {
       return Character.isDigit(value);
    }
    private boolean isLeftParenthesis(char currentCharacter) {
        return currentCharacter =='(';
    }
    private void handleLeftParenthesis(String equation, StringBuilder stringBuilder, int i, char currentCharacter) {
        char prevChar = equation.charAt(i-1);
        if(isDigit(prevChar))
            stringBuilder.insert(i, '*');
        if(isMinusSign(prevChar))
            stringBuilder.insert(i,"1*");
        stringBuilder.append(currentCharacter);
    }
    private boolean isMinusSign(char prevChar) {
        return prevChar == '-';
    }
}
