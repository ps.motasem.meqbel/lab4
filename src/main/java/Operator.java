import java.util.function.BinaryOperator;

public class Operator {
    private String name;
    private int priority;
    // TODO this is a strategy design pattern
    private BinaryOperator<Double> applier;

    public Operator(String name, int priority, BinaryOperator<Double> applier) {
        this.name = name;
        this.priority = priority;
        this.applier = applier;
    }

    public int getPriority() {
        return priority;
    }

    @Override
    public String toString() {
        return this.name;
    }

    public double apply(double leftOperand, double rightOperand) {
        return applier.apply(leftOperand, rightOperand);
    }
}

