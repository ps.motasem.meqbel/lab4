import java.util.LinkedList;
import java.util.Queue;
import java.util.Stack;

public class EquationProcessor {
    private final Stack<Double> operands;
    private final Stack<Operator> operators;
    private Queue<String> equationQueue;

    public EquationProcessor() {
        this.equationQueue = new LinkedList<>();
        this.operands = new Stack<>();
        this.operators = new Stack<>();
    }

    public static void main(String[] args) {
        EquationProcessor processor = new EquationProcessor();
        String equation = "-(-1+23)";
        System.out.println("result: " + processor.execute(equation));
    }

    public String execute(String equation) {
        // TODO throw your own exceptions//done
        if (!isValidEquation(equation)) {
            throw new EquationNotValidException("Syntax Error");//not implemented
        }
        return executeValidEquation(equation);
    }

    private String executeValidEquation(String equation) {
        if (equation.length() == 1) return equation;
        equation = correctEquation(equation);//1(2+1)=>1*(2+1)
        return executeCorrectedEquation(equation);
    }

    private String executeCorrectedEquation(String equation) {
        while (hasParenthesis(equation)) {
            equation = handleParenthesis(equation);
        }
        fillEquationQueue(equation);
        while (!this.equationQueue.isEmpty()) {
            processPart();
        }
        while (!operators.isEmpty()) {
            performNextOperation();
        }
        return operands.pop().toString();
    }

    private String handleParenthesis(String equation) {
        StringBuilder builder = new StringBuilder(equation);
        char[] chars = equation.toCharArray();
        int leftPointer = 0;
        int rightPointer = 0;
        for (int i = 0; i < chars.length; i++) {
            if (chars[i] == '(') {
                leftPointer = i;
            }// (1*(2-4))+(1+9)
            if (chars[i] == ')') {
                rightPointer = i;
                return handleInnerEquation(builder, leftPointer, rightPointer);
            }
        }
        return builder.toString();
    }

    private String handleInnerEquation(StringBuilder equationStringBuilder, int leftPointer, int rightPointer) {
        String innerEquation = equationStringBuilder.substring(leftPointer + 1, rightPointer);
        String innerEquationResult = new EquationProcessor().execute(innerEquation);
        equationStringBuilder.replace(leftPointer, rightPointer + 1, innerEquationResult);
        return equationStringBuilder.toString();
    }

    private boolean hasParenthesis(String equation) {
        return equation.contains("(") || equation.contains(")");
    }

    private String correctEquation(String equation) {
        return new EquationCorrector().correct(equation);
    }

    private void processPart() {
        String element = this.equationQueue.peek();
        if (this.isNumber(element)) {
            this.processNumber(element);
            return;
        }
        processOperator(element);
    }

    private void processOperator(String element) {
        Operator currentOperator = OperatorFactory.create(element);
        if (!this.operators.empty()) {
            calculatePrev(currentOperator);
        }
        this.operators.push(currentOperator);
        this.equationQueue.poll();
    }

    private void calculatePrev(Operator currentOperator) {
        Operator prevOperator = this.operators.peek();
        while (!this.operators.empty() && !isPrevHigher(currentOperator, prevOperator)) {
            performNextOperation();
            prevOperator = this.operators.peek();
        }
    }

    private boolean isPrevHigher(Operator currentOperator, Operator prevOperator) {
        return prevOperator.getPriority() > currentOperator.getPriority();
    }

    private void performNextOperation() {
        if (this.operands.size() == 1)
            return;
        double rightOperand = this.operands.pop();
        double leftOperand = this.operands.pop();
        Operator currentOperation = operators.pop();
        double result = currentOperation.apply(leftOperand, rightOperand);
        this.operands.push(result);
    }

    private void processNumber(String element) {
        this.operands.push(Double.valueOf(element));
        this.equationQueue.poll();
    }

    private boolean isNumber(String value) {
        //return Pattern.matches("\\d*.?\\d*",value);
        try {
            Double.valueOf(value);
            return true;
        } catch (NumberFormatException exception) {
            return false;
        }
    }

    private void fillEquationQueue(String equation) {
        this.equationQueue = new EquationQueueCreator().create(equation);
    }

    private boolean isValidEquation(String equation) {
        return true;
    }
}

