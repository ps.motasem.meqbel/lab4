public class OperatorNotSupportedException extends RuntimeException{
    public OperatorNotSupportedException(String message) {
        super(message);
    }
}
