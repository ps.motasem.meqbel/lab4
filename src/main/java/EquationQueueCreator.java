import java.util.ArrayDeque;
import java.util.LinkedList;
import java.util.Queue;

public class EquationQueueCreator {
    public Queue<String> create(String equation) {
        Queue<String> equationQueue = new ArrayDeque<>();
        char[] array = equation.toCharArray();
        StringBuilder decimalNumberBuilder = new StringBuilder();
        for (int i = 0; i < array.length; i++) {
            char currentChar = array[i];
            if (isOperator(currentChar)) {
                handleOperator(equationQueue, array, decimalNumberBuilder, i);
                continue;
            }
            decimalNumberBuilder.append(array[i]);
        }
        if (isReadyToBeOffered(decimalNumberBuilder))
            equationQueue.offer(decimalNumberBuilder.toString());
        return equationQueue;
    }

    private void handleOperator(Queue<String> equationQueue, char[] array, StringBuilder decimalNumberBuilder, int index) {
        if (isReadyToBeOffered(decimalNumberBuilder)) {
            equationQueue.offer(decimalNumberBuilder.toString());
            rebuild(decimalNumberBuilder);
        }
        if (isNegativeSign(array, index)) {
            handleNegativeSign(equationQueue, array, decimalNumberBuilder, index);
        }else {
            equationQueue.offer(array[index] + "");
        }
    }

    private void handleNegativeSign(Queue<String> equationQueue, char[] array, StringBuilder decimalNumberBuilder, int index) {
        if (index != 0 && isOperator(array[index - 1]) && isParentheses(array[index + 1] + "")) {
            equationQueue.offer(array[index] + "1");
            equationQueue.offer("*");
        }else
            decimalNumberBuilder.append(array[index]);
    }

    private void rebuild(StringBuilder decimalNumberBuilder) {
        decimalNumberBuilder.setLength(0);
    }

    private boolean isReadyToBeOffered(StringBuilder decimalNumberBuilder) {
        return decimalNumberBuilder.length() > 0;
    }

    private boolean isNegativeSign(char[] array, int i) {
        return ((i == 0) && array[0] == '-') || (array[i] == '-') && ((array[i + 1] == '(' || array[i - 1] == '('));
    }

    private boolean isOperator(char c) {
        return c == '(' || c == ')' || c == '+' || c == '-' || c == '*' || c == '/' || c == '^';
    }

    private boolean isParentheses(String element) {
        return element.equals("(") || element.equals(")");
    }

}
