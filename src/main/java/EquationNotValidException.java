public class EquationNotValidException extends RuntimeException{

    public EquationNotValidException(String message) {
        super(message);
    }
}
