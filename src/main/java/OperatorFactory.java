import javax.naming.OperationNotSupportedException;

public class OperatorFactory {
    public static Operator create(String symbol){
        switch (symbol) {
            case "^":
                return new Operator(symbol, 1, Math::pow);
            case "*":
                return new Operator(symbol, 2, (a, b) -> a * b);
            case "/":
                return new Operator(symbol, 3, (a, b) -> a / b);
            case "+":
                return new Operator(symbol, 4, Double::sum);
            case "-":
                return new Operator(symbol, 5, (a, b) -> a - b);
            case "(":
                return new Operator(symbol, 10, (a, b) -> {
                    throw new IllegalStateException("this shouldn't be called");
                });
        }
        throw new OperatorNotSupportedException("Operation Not Supported: "+symbol);
    }
}
